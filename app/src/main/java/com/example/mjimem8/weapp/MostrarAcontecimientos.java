package com.example.mjimem8.weapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MostrarAcontecimientos extends AppCompatActivity {
    private ArrayList<Acontecimiento> items;
    private Button button_atras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_acontecimientos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent AnadirAcontecimientos = new Intent(getApplicationContext(), AnadirAcontecimientos.class);
                startActivity(AnadirAcontecimientos);
            }
        });
        button_atras = (Button) findViewById(R.id.button_atras);
        button_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent AnadirAcontecimientos = new Intent(getApplicationContext(), AnadirAcontecimientos.class);
                startActivity(AnadirAcontecimientos);
            }

        });

        UsuariosSQLiteHelper usdbh =
                new UsuariosSQLiteHelper(this, "Weapp",null,1); //ruta del movil
        SQLiteDatabase db = usdbh.getReadableDatabase();
        SharedPreferences prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String idAcontecimiento = prefs.getString("id","no existe id");
        String[] args = new String[] {idAcontecimiento};
        Cursor c = db.rawQuery("SELECT * FROM acontecimiento where id=?", args);
        items = new ArrayList<Acontecimiento>();
//Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {

                Acontecimiento acontecimiento = new Acontecimiento();
                acontecimiento.setId(Integer.parseInt(c.getString(0)));
                acontecimiento.setNombre(c.getString(1));
                acontecimiento.setOrganizador(c.getString(2));
                acontecimiento.setDescripcion(c.getString(3));
                acontecimiento.setTipo(c.getInt(4));
                acontecimiento.setPortada(c.getString(5));
                acontecimiento.setInicio(c.getString(6));
                acontecimiento.setFin(c.getString(7));
                acontecimiento.setDireccion(c.getString(8));
                acontecimiento.setLocalidad(c.getString(9));
                acontecimiento.setCodigoPostal(c.getInt(10));
                acontecimiento.setProvincia(c.getString(11));
                acontecimiento.setLatitud(c.getInt(12));
                acontecimiento.setLongitud(c.getInt(13));
                acontecimiento.setTelefono(c.getInt(14));
                acontecimiento.setEmail(c.getString(15));
                acontecimiento.setWeb(c.getString(16));
                acontecimiento.setFacebook(c.getString(17));
                acontecimiento.setTwitter(c.getString(18));
                acontecimiento.setInstagram(c.getString(19));
                items.add(acontecimiento);
            } while(c.moveToNext());
        }
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Se crea el Adaptador con los datos
        AcontecimientoAdapter adaptador = new AcontecimientoAdapter(items);

        // Se asocia el elemento con una acción al pulsar el elemento
        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Acción al pulsar el elemento
                int position = recyclerView.getChildAdapterPosition(v);
                Toast toast = Toast.makeText(MostrarAcontecimientos.this, "Posicion: " + String.valueOf(position) + " ID: " + items.get(position).getId() + " Nombre: " + items.get(position).getNombre(), Toast.LENGTH_SHORT);
                toast.show();

            }
        });

        // Se asocia el Adaptador al RecyclerView
        recyclerView.setAdapter(adaptador);

        // Se muestra el RecyclerView en vertical
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}
