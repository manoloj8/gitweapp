package com.example.mjimem8.weapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MostrarEventoFragment extends Fragment {


    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
    }

    public MostrarEventoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mostrar_evento, container, false);
    }

    public void onStart(){
        super.onStart();

        Bundle args = getArguments();
        if(args !=  null){

            updateView(args.getInt("position"));
        }
    }

    public void updateView(int position){
        TextView article = (TextView) getActivity().findViewById(R.id.fragmentTextView);
        article.setText(""+position);
    }


}
