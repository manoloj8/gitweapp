package com.example.mjimem8.weapp;


public class Acontecimiento {
    private int id;
    private String nombre;
    private String organizador;
    private String descripcion;
    private int tipo;
    private String portada;
    private String inicio;
    private String fin;
    private String direccion;
    private String localidad;
    private int codigoPostal;
    private String provincia;
    private float longitud;
    private float latitud;
    private int telefono;
    private String email;
    private String web;
    private String facebook;
    private String twitter;
    private String instagram;

    public Acontecimiento(){

    }

    public Acontecimiento(int id, String nombre, String organizador, String descripcion,
                          int tipo, String portada, String inicio, String fin, String direccion,
                          String localidad, int codigoPostal, String provincia, float longitud,
                          float latitud, int telefono, String email, String web, String facebook,
                          String twitter, String instagram) {
        this.id = id;
        this.nombre = nombre;
        this.organizador = organizador;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.portada = portada;
        this.inicio = inicio;
        this.fin = fin;
        this.direccion = direccion;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
        this.provincia = provincia;
        this.longitud = longitud;
        this.latitud = latitud;
        this.telefono = telefono;
        this.email = email;
        this.web = web;
        this.facebook = facebook;
        this.twitter = twitter;
        this.instagram = instagram;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrganizador() {
        return organizador;
    }

    public void setOrganizador(String organizador) {
        this.organizador = organizador;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getPortada() {
        return portada;
    }

    public void setPortada(String portada) {
        this.portada = portada;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }
}
