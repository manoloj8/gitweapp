package com.example.mjimem8.weapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AcercaDeActivity extends AppCompatActivity {
    private static final String ACTIVITY = "AcerdaDe";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);
    }


    @Override
    protected void onStart() {
        MyLog.d(ACTIVITY, "On Start...");
        super.onStart();
    }

    @Override
    protected void onResume() {
        MyLog.d(ACTIVITY, "On Resume...");
        super.onResume();
    }

    @Override
    protected void onPause() {
        MyLog.d(ACTIVITY, "On Pause...");
        super.onPause();
    }

    @Override
    protected void onStop() {
        MyLog.d(ACTIVITY, "On Stop...");
        super.onStop();

    }

    @Override
    protected void onRestart() {
        MyLog.d(ACTIVITY, "On Restart...");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        MyLog.d(ACTIVITY, "On Destroy...");
        super.onDestroy();
    }
}
