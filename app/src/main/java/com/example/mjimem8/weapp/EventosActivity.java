package com.example.mjimem8.weapp;


import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class EventosActivity extends AppCompatActivity implements ListadosEventoFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos);

        if(findViewById(R.id.unique_fragment) != null){
            MostrarEventoFragment listadoFrag = new MostrarEventoFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.unique_fragment, listadoFrag).commit();
        }
    }

    @Override
    public void onFragmentInteraction(int position) {

        MostrarEventoFragment contenidoFrag = (MostrarEventoFragment)
                getSupportFragmentManager().findFragmentById(R.id.contenido_fragment);

        if(contenidoFrag != null){

            contenidoFrag.updateView(position);
        }else{

            MostrarEventoFragment newcontenidoFrag = new MostrarEventoFragment();
            Bundle args = new Bundle();
            args.putInt("position", position);
            newcontenidoFrag.setArguments(args);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.unique_fragment, newcontenidoFrag);
            transaction.addToBackStack(null);

            transaction.commit();
        }
    }
}
