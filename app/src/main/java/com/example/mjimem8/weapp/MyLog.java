package com.example.mjimem8.weapp;

/**
 * Created by mjimem8 on 04/10/2016.
 */

public class MyLog {
    static final boolean DEBUG = true;

    public static void i(String tag, String string) {
        if (DEBUG) android.util.Log.i(tag, string);
    }
    public static void e(String tag, String string) {
        if (DEBUG) android.util.Log.e(tag, string);
    }
    public static void d(String tag, String string) {
        if (DEBUG) android.util.Log.d(tag, string);
    }
    public static void v(String tag, String string) {
        if (DEBUG) android.util.Log.v(tag, string);
    }
    public static void w(String tag, String string) {
        if (DEBUG) android.util.Log.w(tag, string);
    }
}


