
package com.example.mjimem8.weapp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


/**
 *
 * @author Mayik
 */
public class AcontecimientoDAO {
    private Conexion conexion = new Conexion();
    private Connection con;
    private Statement st;
    private ResultSet rs;

    public AcontecimientoDAO() {
        try{
            if((con = conexion.getConexionMYSQL())==null){
                return;
            }
            st = con.createStatement();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean insert(String nombre,String genero,int anio,String actor,String pais){
        try {
            String query = "INSERT INTO peliculas (NOMBRE,GENERO,ANIO,ACTOR,PAIS) VALUES('"+nombre+"','"+genero+"','"+anio+"','"+actor+"','"+pais+"');";
            st.executeUpdate(query);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public ResultSet selectAll(){
        try {
            String query = "SELECT * FROM acontecimiento";
            rs = st.executeQuery(query);
            return rs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public boolean delete(int id){
        try {
            String query = "DELETE FROM peliculas WHERE ID = '"+id+"'";
            st.executeUpdate(query);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(int id,String nombre,String genero,int anio,String actor,String pais){
        try {
            String query = "UPDATE peliculas SET"
                    + " NOMBRE = '"+nombre+"',"
                    + " GENERO = '"+genero+"',"
                    + " ANIO = '"+anio+"',"
                    + " ACTOR = '"+actor+"',"
                    + " PAIS = '"+pais+"' WHERE ID = '"+id+"';";
            st.executeUpdate(query);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
