package com.example.mjimem8.weapp;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by mjimem8 on 08/11/2016.
 */

public class UsuariosSQLiteHelper extends SQLiteOpenHelper {

    String sqlCreate = "CREATE TABLE IF NOT EXISTS `acontecimiento` (\n" +
            "  `id` int(11) NOT NULL,\n" +
            "  `nombre` varchar(256)  NOT NULL,\n" +
            "  `organizador` varchar(256)  DEFAULT NULL,\n" +
            "  `descripcion` varchar(1024)  NOT NULL,\n" +
            "  `tipo` int(11) NOT NULL,\n" +
            "  `portada` varchar(256)  DEFAULT NULL,\n" +
            "  `inicio` datetime NOT NULL,\n" +
            "  `fin` datetime NOT NULL,\n" +
            "  `direccion` varchar(256)  DEFAULT NULL,\n" +
            "  `localidad` varchar(256)  DEFAULT NULL,\n" +
            "  `cod_postal` int(5) DEFAULT NULL,\n" +
            "  `provincia` varchar(256)  DEFAULT NULL,\n" +
            "  `longitud` float DEFAULT NULL,\n" +
            "  `latitud` float DEFAULT NULL,\n" +
            "  `telefono` int(9) DEFAULT NULL,\n" +
            "  `email` varchar(256)  DEFAULT NULL,\n" +
            "  `web` varchar(256)  DEFAULT NULL,\n" +
            "  `facebook` varchar(256)  DEFAULT NULL,\n" +
            "  `twitter` varchar(256)  DEFAULT NULL,\n" +
            "  `instagram` varchar(256)  DEFAULT NULL,\n" +
            "   PRIMARY KEY (`id`)\n" +
            "   );";
    String sqlCreate1 = "CREATE TABLE IF NOT EXISTS `evento` (\n" +
            "  `id` int(11) NOT NULL,\n" +
            "  `id_acontecimiento` int(11) DEFAULT NULL,\n" +
            "  `nombre` varchar(256)  NOT NULL,\n" +
            "  `descripcion` varchar(2014)  NOT NULL,\n" +
            "  `inicio` datetime NOT NULL,\n" +
            "  `fin` datetime NOT NULL,\n" +
            "  `direccion` varchar(256)  DEFAULT NULL,\n" +
            "  `localidad` varchar(256)  DEFAULT NULL,\n" +
            "  `cod_postal` int(5) DEFAULT NULL,\n" +
            "  `provincia` varchar(256)  DEFAULT NULL,\n" +
            "  `longitud` float DEFAULT NULL,\n" +
            "  `latitud` float DEFAULT NULL,\n" +
            "   PRIMARY KEY (`id`),\n" +
            "   FOREIGN KEY(id_acontecimiento) REFERENCES acontecimiento(id)\n" +
            "   );";

    public UsuariosSQLiteHelper(Context contexto, String nombre,
                                SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(sqlCreate);
        db.execSQL(sqlCreate1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
        db.execSQL("DROP TABLE IF EXISTS acontecimiento");
        db.execSQL("DROP TABLE IF EXISTS evento");
        db.execSQL(sqlCreate);
        db.execSQL(sqlCreate1);
    }


}
