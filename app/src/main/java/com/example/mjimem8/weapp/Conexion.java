package com.example.mjimem8.weapp;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Mayik
 */
public class Conexion{
    //Configuracion de los datos de la BD
    private String usuario = "prueba";
    private String pass = "prueba";
    private String host = "127.0.0.1";
    private String nombre_BD = "prueba";
    private String puerto = "3306";

    private Connection con = null;

    public Conexion() {
    }
    //Metodo que se devuelve la conexion o null si hubo un error
    public Connection getConexionMYSQL(){
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String servidor = "jdbc:mysql://" + host + ":" + puerto + "/" + nombre_BD;
            con = DriverManager.getConnection(servidor,usuario,pass);
            return con;
        }catch(Exception e){
            e.printStackTrace();
            return con;
        }
    }
}
