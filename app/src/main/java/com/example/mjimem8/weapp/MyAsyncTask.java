package com.example.mjimem8.weapp;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

/**
 * Created by mjimem8 on 25/10/2016.
 */

public class MyAsyncTask extends AsyncTask<String, String, String> { //variables que se van a pasar al doinbackground, podria ser x y etc
    HttpURLConnection urlConnection;
    StringBuilder result = new StringBuilder();
    ProgressBar pb;


    String id;
    Context contexto;

    public MyAsyncTask(String id, Context contexto, ProgressBar pb) {
        this.id = id;
        this.contexto = contexto;
        this.pb = pb;

    }

    protected void onPreExecute() {
        pb.setVisibility(View.VISIBLE);
    }


    public HttpURLConnection getUrlConnection() {
        return urlConnection;
    }

    public void setUrlConnection(HttpURLConnection urlConnection) {
        this.urlConnection = urlConnection;
    }

    @Override
    protected String doInBackground(String... args) {

        try {
            URL url = new URL("http://weapp.hol.es/api/v1/acontecimiento/"+id);
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            try {
                JSONObject acontecimientoJson = new JSONObject(result.toString());
                UsuariosSQLiteHelper usdbh =
                        new UsuariosSQLiteHelper(contexto, "Weapp", null, 1); //ruta del movil
                SQLiteDatabase db = usdbh.getWritableDatabase();
                //Le daremos permiso a la Base de Datos para que pueda escribir
                if (acontecimientoJson.has("acontecimiento")) {
                    MyLog.e("tag", "contiene acontecimiento");
                    JSONObject objetoJsonInterno = new JSONObject(acontecimientoJson.getString("acontecimiento"));

                    if (db != null) {
                        //Comprobamos todos los campos del acontecimiento
                        String nombreAcontecimiento = (objetoJsonInterno.has("nombre")) ? objetoJsonInterno.getString("nombre") : "";
                        String nombreOrganizador = (objetoJsonInterno.has("organizador")) ? objetoJsonInterno.getString("organizador") : "";
                        String descripcion = (objetoJsonInterno.has("descripcion")) ? objetoJsonInterno.getString("descripcion") : "";
                        String tipo = (objetoJsonInterno.has("tipo")) ? objetoJsonInterno.getString("tipo") : "";
                        String portada = (objetoJsonInterno.has("portada")) ? objetoJsonInterno.getString("portada") : "";
                        String inicio = (objetoJsonInterno.has("inicio")) ? objetoJsonInterno.getString("inicio") : "";
                        String fin = (objetoJsonInterno.has("fin")) ? objetoJsonInterno.getString("fin") : "";
                        String direccion = (objetoJsonInterno.has("direccion")) ? objetoJsonInterno.getString("direccion") : "";
                        String localidad = (objetoJsonInterno.has("localidad")) ? objetoJsonInterno.getString("localidad") : "";
                        String cod_postal = (objetoJsonInterno.has("cod_postal")) ? objetoJsonInterno.getString("cod_postal") : "";
                        String provincia = (objetoJsonInterno.has("provincia")) ? objetoJsonInterno.getString("provincia") : "";
                        String latitud = (objetoJsonInterno.has("latitud")) ? objetoJsonInterno.getString("latitud") : "";
                        String longitud = (objetoJsonInterno.has("longitud")) ? objetoJsonInterno.getString("longitud") : "";
                        String telefono = (objetoJsonInterno.has("telefono")) ? objetoJsonInterno.getString("telefono") : "";
                        String email = (objetoJsonInterno.has("email")) ? objetoJsonInterno.getString("email") : "";
                        String web = (objetoJsonInterno.has("web")) ? objetoJsonInterno.getString("web") : "";
                        String facebook = (objetoJsonInterno.has("facebook")) ? objetoJsonInterno.getString("facebook") : "";
                        String twitter = (objetoJsonInterno.has("twitter")) ? objetoJsonInterno.getString("twitter") : "";
                        String instagram = (objetoJsonInterno.has("instagram")) ? objetoJsonInterno.getString("instagram") : "";

                        //Insertamos el acontecimiento en la base de datos
                        ContentValues nuevoRegistro = new ContentValues();
                        //El id es el que introduciremos en el movil
                        nuevoRegistro.put("id", id);
                        nuevoRegistro.put("nombre", nombreAcontecimiento);
                        nuevoRegistro.put("organizador", nombreOrganizador);
                        nuevoRegistro.put("descripcion", descripcion);
                        nuevoRegistro.put("tipo", tipo);
                        nuevoRegistro.put("portada", portada);
                        nuevoRegistro.put("inicio", inicio);
                        nuevoRegistro.put("fin", fin);
                        nuevoRegistro.put("direccion", direccion);
                        nuevoRegistro.put("localidad", localidad);
                        nuevoRegistro.put("cod_postal", cod_postal);
                        nuevoRegistro.put("provincia", provincia);
                        nuevoRegistro.put("latitud", latitud);
                        nuevoRegistro.put("longitud", longitud);
                        nuevoRegistro.put("telefono", telefono);
                        nuevoRegistro.put("email", email);
                        nuevoRegistro.put("web", web);
                        nuevoRegistro.put("facebook", facebook);
                        nuevoRegistro.put("twitter", twitter);
                        nuevoRegistro.put("instagram", instagram);
                        db.delete("acontecimiento", "id=" + id, null);
                        db.insert("acontecimiento", null, nuevoRegistro);


                        if (acontecimientoJson.has("evento")) {
                            //Creo un array de la lista de los eventos
                            JSONArray listaeventos = new JSONArray(acontecimientoJson.getString("evento"));

                            for (int i = 0; i < listaeventos.length(); i++) {
                                MyLog.e("tag", "contiene evento");
                                //Comprobamos todos los campos del evento
                                String nombreAcontecimientoEvento = (objetoJsonInterno.has("nombre")) ? objetoJsonInterno.getString("nombre") : "";
                                String descripcionEvento = (objetoJsonInterno.has("descripcion")) ? objetoJsonInterno.getString("descripcion") : "";
                                String inicioEvento = (objetoJsonInterno.has("inicio")) ? objetoJsonInterno.getString("inicio") : "";
                                String finEvento = (objetoJsonInterno.has("fin")) ? objetoJsonInterno.getString("fin") : "";
                                String direccionEvento = (objetoJsonInterno.has("direccion")) ? objetoJsonInterno.getString("direccion") : "";
                                String localidadEvento = (objetoJsonInterno.has("localidad")) ? objetoJsonInterno.getString("localidad") : "";
                                String cod_postalEvento = (objetoJsonInterno.has("cod_postal")) ? objetoJsonInterno.getString("cod_postal") : "";
                                String provinciaEvento = (objetoJsonInterno.has("provincia")) ? objetoJsonInterno.getString("provincia") : "";
                                String latitudEvento = (objetoJsonInterno.has("latitud")) ? objetoJsonInterno.getString("latitud") : "";
                                String longitudEvento = (objetoJsonInterno.has("longitud")) ? objetoJsonInterno.getString("longitud") : "";

                                //Inserto los datos del evento
                                ContentValues nuevoRegistroEvento = new ContentValues();
                                nuevoRegistroEvento.put("id_acontecimiento", id);
                                nuevoRegistroEvento.put("nombre", nombreAcontecimientoEvento);
                                nuevoRegistroEvento.put("descripcion", descripcionEvento);
                                nuevoRegistroEvento.put("inicio", inicioEvento);
                                nuevoRegistroEvento.put("fin", finEvento);
                                nuevoRegistroEvento.put("direccion", direccionEvento);
                                nuevoRegistroEvento.put("localidad", localidadEvento);
                                nuevoRegistroEvento.put("cod_postal", cod_postalEvento);
                                nuevoRegistroEvento.put("provincia", provinciaEvento);
                                nuevoRegistroEvento.put("latitud", latitudEvento);
                                nuevoRegistroEvento.put("longitud", longitudEvento);
                                db.insert("evento", null, nuevoRegistroEvento);

                            }
                            db.close();
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            urlConnection.disconnect();
        }

        return result.toString();

    }

    @Override
    protected void onPostExecute(String result) {

        pb.setVisibility(View.INVISIBLE);

        SharedPreferences prefs = contexto.getSharedPreferences("Preferences", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("id", id);
        editor.commit();
        Intent intent = new Intent(contexto, MostrarAcontecimientos.class);
        contexto.startActivity(intent);
        ((Activity)contexto).finish();

    }

    //protected void onProgressUpdate(String string){
}




