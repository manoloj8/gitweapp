package com.example.mjimem8.weapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import android.database.Cursor;

import java.util.ArrayList;

public class ListadoAcontecimientos extends AppCompatActivity {
    private static final String ACTIVITY = "FirstActivity";
    private ArrayList<Acontecimiento> items;
    private AcontecimientoDAO acontecimientoDAO;

    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_acontecimientos);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        progressBar.setMax(100);
        progressBar.setBackgroundColor(Color.GRAY);
        progressBar.setProgress(0);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent AnadirAcontecimientos = new Intent(getApplicationContext(), AnadirAcontecimientos.class);
                startActivity(AnadirAcontecimientos);
            }
        });

        TextView textoBienvenida = (TextView)findViewById(R.id.textoBienvenida);
        textoBienvenida.setText("Pulsa el botón de abajo para buscar un acontecimiento");




        /*
        acontecimientoDAO = new AcontecimientoDAO();
        ResultSet rs = acontecimientoDAO.selectAll();
        items = new ArrayList<Acontecimiento>();
        if(rs!=null) {
            try {
                while (rs.next()) {
                    Object row[] = new Object[2];
                    for (int i = 0; i < 2; i++) {
                        row[i] = rs.getObject(i + 1); //El resulset los indices empiezan en 1, mi for lo empiezo en 0 por eso le sumo i+1
                    }
                    Acontecimiento acontecimiento = new Acontecimiento();
                    acontecimiento.setId(Integer.parseInt(row[0].toString()));
                    acontecimiento.setNombre(row[1].toString());
                    items.add(acontecimiento);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        */
        // Se inicializa el RecyclerView
        items = new ArrayList<Acontecimiento>();
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Se crea el Adaptador con los datos
        AcontecimientoAdapter adaptador = new AcontecimientoAdapter(items);

        // Se asocia el elemento con una acción al pulsar el elemento
        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Acción al pulsar el elemento
                    int position = recyclerView.getChildAdapterPosition(v);
                    Toast toast = Toast.makeText(ListadoAcontecimientos.this, "Posicion: " + String.valueOf(position) + " ID: " + items.get(position).getId() + " Nombre: " + items.get(position).getNombre(), Toast.LENGTH_SHORT);
                    toast.show();

            }
        });

        // Se asocia el Adaptador al RecyclerView
        recyclerView.setAdapter(adaptador);

        // Se muestra el RecyclerView en vertical
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //new MyAsyncTask("1",this,progressBar).execute();


    }

    @Override
    protected void onStart() {
        MyLog.d(ACTIVITY, "On Start...");
        super.onStart();
    }

    @Override
    protected void onResume() {
        MyLog.d(ACTIVITY, "On Resume...");
        super.onResume();
    }

    @Override
    protected void onPause() {
        MyLog.d(ACTIVITY, "On Pause...");
        super.onPause();
    }

    @Override
    protected void onStop() {
        MyLog.d(ACTIVITY, "On Stop...");
        super.onStop();

    }

    @Override
    protected void onRestart() {
        MyLog.d(ACTIVITY, "On Restart...");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        MyLog.d(ACTIVITY, "On Destroy...");
        super.onDestroy();
    }
}





