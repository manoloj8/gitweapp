package com.example.mjimem8.weapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjimem8 on 18/10/2016.
 */

public class AcontecimientoAdapter
        extends RecyclerView.Adapter<AcontecimientoAdapter.AcontecimientoViewHolder>
        implements View.OnClickListener {

    private ArrayList<Acontecimiento> items;
    private View.OnClickListener listener;

    // Clase interna:
    // Se implementa el ViewHolder que se encargará
    // de almacenar la vista del elemento y sus datos
    public static class AcontecimientoViewHolder
            extends RecyclerView.ViewHolder {

        private TextView TextView_nombre;
        private TextView TextView_id;
        private TextView TextView_organizador;
        private TextView TextView_descripcion;
        private TextView TextView_tipo;
        private TextView TextView_portada;
        private TextView TextView_inicio;
        private TextView TextView_fin;
        private TextView TextView_direccion;
        private TextView TextView_localidad;
        private TextView TextView_cod_postal;
        private TextView TextView_provincia;
        private TextView TextView_latitud;
        private TextView TextView_longitud;
        private TextView TextView_telefono;
        private TextView TextView_email;
        private TextView TextView_web;
        private TextView TextView_facebook;
        private TextView TextView_twitter;
        private TextView TextView_instagram;


        public AcontecimientoViewHolder(View itemView) {
            super(itemView);
            TextView_id = (TextView) itemView.findViewById(R.id.TextView_id);
            TextView_nombre = (TextView) itemView.findViewById(R.id.TextView_nombre);
            TextView_organizador = (TextView) itemView.findViewById(R.id.TextView_organizador);
            TextView_descripcion = (TextView) itemView.findViewById(R.id.TextView_descripcion);
            TextView_tipo = (TextView) itemView.findViewById(R.id.TextView_tipo);
            TextView_portada = (TextView) itemView.findViewById(R.id.TextView_portada);
            TextView_inicio = (TextView) itemView.findViewById(R.id.TextView_inicio);
            TextView_fin = (TextView) itemView.findViewById(R.id.TextView_fin);
            TextView_direccion = (TextView) itemView.findViewById(R.id.TextView_direccion);
            TextView_localidad = (TextView) itemView.findViewById(R.id.TextView_localidad);
            TextView_cod_postal = (TextView) itemView.findViewById(R.id.TextView_cod_postal);
            TextView_provincia = (TextView) itemView.findViewById(R.id.TextView_provincia);
            TextView_latitud = (TextView) itemView.findViewById(R.id.TextView_latitud);
            TextView_longitud = (TextView) itemView.findViewById(R.id.TextView_longitud);
            TextView_telefono = (TextView) itemView.findViewById(R.id.TextView_telefono);
            TextView_email = (TextView) itemView.findViewById(R.id.TextView_email);
            TextView_web = (TextView) itemView.findViewById(R.id.TextView_web);
            TextView_facebook = (TextView) itemView.findViewById(R.id.TextView_facebook);
            TextView_twitter = (TextView) itemView.findViewById(R.id.TextView_twitter);
            TextView_instagram = (TextView) itemView.findViewById(R.id.TextView_instagram);
        }

        public void AcontecimientoBind(Acontecimiento item) {
            TextView_id.setText(String.valueOf(item.getId()));
            TextView_nombre.setText(item.getNombre());
            TextView_organizador.setText(item.getOrganizador());
            TextView_descripcion.setText(item.getDescripcion());
            TextView_tipo.setText(String.valueOf(item.getTipo()));
            TextView_portada.setText(item.getPortada());
            TextView_inicio.setText(item.getInicio());
            TextView_fin.setText(item.getFin());
            TextView_direccion.setText(item.getDireccion());
            TextView_localidad.setText(item.getLocalidad());
            TextView_cod_postal.setText(String.valueOf(item.getCodigoPostal()));
            TextView_provincia.setText(item.getProvincia());
            TextView_latitud.setText(String.valueOf(item.getLatitud()));
            TextView_longitud.setText(String.valueOf(item.getLongitud()));
            TextView_telefono.setText(String.valueOf(item.getTelefono()));
            TextView_email.setText(item.getEmail());
            TextView_web.setText(item.getWeb());
            TextView_facebook.setText(item.getFacebook());
            TextView_twitter.setText(item.getTwitter());
            TextView_instagram.setText(item.getInstagram());
        }
    }

    // Contruye el objeto adaptador recibiendo la lista de datos
    public AcontecimientoAdapter(@NonNull ArrayList<Acontecimiento> items) {
        this.items = items;
    }

    // Se encarga de crear los nuevos objetos ViewHolder necesarios para los elementos de la colección.
    // Infla la vista del layout y crea y devuelve el objeto ViewHolder
    @Override
    public AcontecimientoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_listado_acontecimiento, parent, false);
        row.setOnClickListener(this);
        AcontecimientoViewHolder avh = new AcontecimientoViewHolder(row);
        return avh;
    }

    // Se encarga de actualizar los datos de un ViewHolder ya existente.
    @Override
    public void onBindViewHolder(AcontecimientoViewHolder viewHolder, int position) {
        Acontecimiento item = items.get(position);
        viewHolder.AcontecimientoBind(item);
    }

    // Indica el número de elementos de la colección de datos.
    @Override
    public int getItemCount() {
        return items.size();
    }

    // Asigna un listener
    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }
}
